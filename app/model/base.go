package model

type Base struct {
	ID         int   `gorm:"primary_key" json:"id"`
	CreateTime int64 `gorm:"autoCreateTime" json:"create_time"`
	UpdateTime int64 `gorm:"autoUpdateTime" json:"update_time"`
	DeleteTime int64 `gorm:"autoUpdateTime" json:"delete_time"`
}
