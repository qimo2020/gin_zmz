package model

type UserRed struct {
	Id			int    	 `gorm:"primary_key" json:"id"`
	UserId		int   	 `json:"user_id"`
	RedPackId	int   	 `json:"red_pack_id"`
	CreateTime  int64    `json:"create_time"`
	UpdateTime  int64    `json:"update_time"`
}