package model

type RedPack struct {
	Id			int    	 `gorm:"primary_key" json:"id"`
	Name		string   `json:"name"`
	Stock		int	 	 `json:"stock"`
	CreateTime  int64    `json:"create_time"`
	UpdateTime  int64    `json:"update_time"`
}