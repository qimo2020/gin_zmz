package model

import (
	"strconv"

	"gorm.io/gorm"
)

type User struct {
	Base
	Nickname   string `json:"nickname"`
	Headimgurl string `json:"headimgurl"`
	Username   string `json:"username"`
	Password   string `json:"password"`
}

type UserList struct {
	Base
	Nickname string `json:"nickname"`
	Username string `json:"username"`
	Test     string `json:"test" gorm:"-"`
}

type UserInfo struct {
	Base
	Nickname   string `json:"nickname"`
	Headimgurl string `json:"headimgurl"`
}

func (u *User) AfterFind(tx *gorm.DB) (err error) {
	return
}

func (u *UserList) AfterFind(tx *gorm.DB) (err error) {
	u.Test = "test" + strconv.Itoa(u.ID)
	return
}
