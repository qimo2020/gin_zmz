package upload

import(
	"github.com/gin-gonic/gin"
	"gin_zmz/boot"
	"gin_zmz/utils/response"
	"gin_zmz/utils/helper"
	"strconv"
	"path"
	"time"
	"fmt"
	"os"
)

func Upload(c *gin.Context){

	file,err := c.FormFile("file")

	if err != nil{
		c.JSON(200,response.Data(10301,"",err.Error()))
		return
	}
	limit_size := int64(1048576*boot.Upload_MaxSize)
	if(file.Size>limit_size){
		c.JSON(200,response.Data(10301,"文件大小超出限制，限制大小为"+strconv.Itoa(boot.Upload_MaxSize)+"MB",nil))
		return
	}
	file_suffix := path.Ext(file.Filename)

	time_path := time.Now().Format("200601")
	file_path := boot.Upload_Path+time_path
	if helper.FileIsExist(file_path) == false{
		os.Mkdir(file_path, os.ModePerm)
	}
	path := file_path+"/"+helper.RandStr(8)+file_suffix
	fmt.Println(path)
	c.SaveUploadedFile(file,path)
	c.JSON(200,response.Data(0,"成功",nil))
	return
}