package red_pack

import(
	"github.com/gin-gonic/gin"
	"gin_zmz/app/service/red_pack"
	"gin_zmz/utils/response"
)

func GetRedPack(c *gin.Context){
	var params GetRedPackParams
	if err := c.ShouldBind(&params); err != nil {
		c.JSON(200,response.ErrParam)
		return
	}
	if(params.UserId==0||params.RedPackId==0){
		c.JSON(200,response.ErrParam)
		return
	}
	status,msg := red_pack.GetRedPack(params.UserId,params.RedPackId)
	if(status == false){
		c.JSON(200,response.ErrCustom.WithMsg(msg.Error()))
		return
	}
	c.JSON(200,response.OK)
	return
}


func CreateRedPack(c *gin.Context){
	var params CreateRedPackParams
	if err := c.ShouldBind(&params); err != nil {
		c.JSON(200,response.ErrParam)
		return
	}
	red_pack.CreateRedPack(params.Name,params.Stock)
	c.JSON(200,response.OK)
	return
}