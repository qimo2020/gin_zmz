package red_pack

type GetRedPackParams struct{
	UserId 		int `form:"user_id" binding:"required|number" json:"user_id"`
	RedPackId	int `form:"red_pack_id" binding:"required|number" json:"red_pack_id"`
}

type CreateRedPackParams struct {
	Name		string `form:"name" binding:"required" json:"name"`
	Stock	    int    `form:"stock" binding:"required|number" json:"stock"`
}