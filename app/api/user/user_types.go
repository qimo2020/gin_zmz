package user

type UserRegist struct{
	Username 	string	`form:"username" binding:"required"`
	Password 	string	`form:"password" binding:"required"`
	Nickname 	string	`form:"nickname" binding:"required"`
}

type UserLogin  struct{
	Username 	string	`form:"username" binding:"required" json:"username"`
	Password 	string	`form:"password" binding:"required" json:"password"`
}

type UserId struct{
	UserId			int   `form:"user_id" binding:"required|number"`
}

type SendMessageParams struct{
	UserId		int   	`form:"id" binding:"required|number" json:"user_id"`
	Data		string	`form:"data" binding:"required|string" json:"data"`
}

type SendBroadcastParams struct{
	UserId		int 	`form:"user_id" binding:"required|number" json:"user_id"`
	Data		string	`form:"data" binding:"required" json:"data"`
}