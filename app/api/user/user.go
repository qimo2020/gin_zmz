package user

import (
	"gin_zmz/app/model"
	"gin_zmz/app/service/user"
	"gin_zmz/app/service/websocket"
	"gin_zmz/boot"
	"gin_zmz/utils/helper"
	"gin_zmz/utils/response"

	"github.com/gin-gonic/gin"
)

func Index(c *gin.Context) {
	var user []model.UserList
	data := boot.Db.Model(&model.User{}).Find(&user)
	count := data.RowsAffected
	pagination := helper.BuildPage(1, 10, int(count))
	c.JSON(200, response.OK.WithData(map[string]interface{}{"list": user, "pagination": pagination}))
	return
}

func Regist(c *gin.Context) {
	var rgData UserRegist
	if err := c.ShouldBind(&rgData); err != nil {
		c.JSON(200, response.Data(10101, "参数错误", nil))
		return
	}

	errs := user.Regist(c.PostForm("username"), c.PostForm("password"), c.PostForm("nickname"))
	if errs != nil {
		c.JSON(200, response.Data(10301, errs.Error(), nil))
		return
	}

	c.JSON(200, response.Data(0, "成功", nil))
	return
}

func Login(c *gin.Context) {
	var lgData UserLogin
	if err := c.ShouldBind(&lgData); err != nil {
		c.JSON(200, response.Data(10101, "参数错误", nil))
		return
	}
	token, errs := user.Login(lgData.Username, lgData.Password)
	if errs != nil {
		c.JSON(200, response.Data(10301, errs.Error(), nil))
		return
	}
	c.JSON(200, response.OK.WithData(token))
	return
}

func Info(c *gin.Context) {
	user_id, _ := c.Get("user_id")
	info := user.Info(user_id.(int))
	c.JSON(200, response.OK.WithData(info))
}

func SendMessage(c *gin.Context) {
	var sendMessageParams SendMessageParams
	send_user_id, _ := c.Get("user_id")
	if err := c.ShouldBind(&sendMessageParams); err != nil {
		c.JSON(200, response.Data(10101, "参数错误", nil))
	}
	websocket.SendMessageClient(send_user_id.(int), sendMessageParams.UserId, sendMessageParams.Data)
	c.JSON(200, response.OK)
	return
}

func SendBroadcast(c *gin.Context) {
	var broadcase SendBroadcastParams
	if err := c.ShouldBind(&broadcase); err != nil {
		c.JSON(200, response.Data(10101, "参数错误", nil))
		return
	}

	user_info := user.Info(broadcase.UserId)
	msg_type := "comment"
	data := make(map[string]string)
	data["nickname"] = user_info.Nickname
	data["headimgurl"] = user_info.Headimgurl
	data["msg"] = broadcase.Data
	websocket.SendBroadcast(msg_type, data)
	c.JSON(200, response.Data(0, "成功", data))
	return
}
