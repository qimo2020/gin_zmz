package websocket

import "fmt"

type Test interface {
	Eat()
	// Seelp()
}

type Person struct {
	Name string
	Age  int
}

func NewPerson(name string, age int) *Person {
	return &Person{
		Name: name,
		Age:  age,
	}
}

func (p *Person) Write() {
	fmt.Println(p.Name)
}

func (p *Person) Eat() {
	fmt.Println(p.Name + "吃苹果")
}
