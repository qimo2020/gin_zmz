package user

import (
	"errors"
	"gin_zmz/app/model"
	"gin_zmz/boot"
	"gin_zmz/utils/jwt"
)

func Regist(username string, password string, nickname string) error {
	data := boot.Db.Where("username = ?", username).First(&model.User{})
	if data.RowsAffected != 0 {
		return errors.New("已存在此用户名,请重新添写")
	}
	user := model.User{
		Username: username,
		Password: password,
		Nickname: nickname,
	}
	boot.Db.Create(&user)

	return nil
}

func Login(username string, password string) (string, error) {
	var user model.User
	boot.Db.Where("username = ? AND password = ?", username, password).First(&user)
	if user.ID == 0 {
		return "", errors.New("账号或密码错误")
	}
	token, err := jwt.Setting(user.ID)
	if err != nil {
		return "", err
	}
	return token, nil

}

func Info(user_id int) model.User {
	var user model.User
	boot.Db.Where("id = ?", user_id).First(&user)
	return user
}
