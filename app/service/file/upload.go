package file

import (
	"gin_zmz/boot"
	"gin_zmz/utils/helper"
	"gin_zmz/utils/response"
	"os"
	"path"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

func UploadLocal(c *gin.Context) {

	file, err := c.FormFile("file")

	if err != nil {
		c.JSON(200, response.ErrFile.WithMsg(err.Error()))
		return
	}

	//限制大小
	limitSize := int64(1048576 * boot.Upload_MaxSize)

	if file.Size > limitSize {
		c.JSON(200, response.ErrFile.WithMsg("文件大小超出限制，限制大小"+strconv.Itoa(boot.Upload_MaxSize)+"MB"))
		return
	}
	fileSuffix := path.Ext(file.Filename)
	timePath := time.Now().Format("200601")
	filePath := boot.Upload_Path + timePath
	if helper.FileIsExist(filePath) == false {
		os.Mkdir(filePath, os.ModePerm)
	}

	path := filePath + "/" + strconv.FormatInt(time.Now().Unix(), 11) + fileSuffix
	c.SaveUploadedFile(file, path)

	c.JSON(200, response.OK.WithData(map[string]interface{}{"id": 1, "url": path}))
	return
}
