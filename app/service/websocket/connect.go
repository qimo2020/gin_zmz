package websocket

import (
	"math/rand"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

const (
	maxMessageSize = 8192
)

func Run(c *gin.Context) {

	conn, _ := (&websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		//处理跨域
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}).Upgrade(c.Writer, c.Request, nil)

	//设置读取消息大小上线
	conn.SetReadLimit(maxMessageSize)

	user_id := rand.Int()

	clientSocket := NewClient(user_id, conn)

	go clientSocket.Read()

	//用户连接
	Manager.Connect <- clientSocket
}
