package websocket

import (
	"errors"
	"fmt"
)

type ClientManager struct {
	Connect    chan *Client //处理连接请求
	DisConnect chan *Client //处理断开请求

	ClientMap map[int]*Client //所有的客户端

}

func NewClientManager() *ClientManager {
	return &ClientManager{
		Connect:    make(chan *Client, 1000),
		DisConnect: make(chan *Client, 1000),
		ClientMap:  make(map[int]*Client),
	}
}

func (c *ClientManager) Start() {
	for {
		select {
		case addclient := <-c.Connect:
			c.AddClient(addclient)
		case delclient := <-c.DisConnect:
			c.DelClient(delclient)
		default:

		}
	}
}

func (c *ClientManager) AddClient(client *Client) {
	c.ClientMap[client.UserId] = client
}

func (c *ClientManager) DelClient(client *Client) {
	client.Socket.Close()
	delete(c.ClientMap, client.UserId)
	fmt.Println("客户端断开：%s", client.UserId)
	client = nil
}

// 通过userId获取
func (c *ClientManager) GetByUserId(user_id int) (*Client, error) {
	if client, ok := c.ClientMap[user_id]; !ok {
		return nil, errors.New("客户端不存在")
	} else {
		return client, nil
	}
}

func (c *ClientManager) GetAllUser() map[int]*Client {
	return c.ClientMap
}
