package websocket

import (
	"time"

	"github.com/gorilla/websocket"
)

type Client struct {
	Socket      *websocket.Conn
	ConnectTime int64
	UserId      int
}

func NewClient(userId int, socket *websocket.Conn) *Client {
	return &Client{
		Socket:      socket,
		ConnectTime: time.Now().Unix(),
		UserId:      userId,
	}
}

func (c *Client) Read() {
	go func() {
		messageType, _, err := c.Socket.ReadMessage()
		if err != nil {
			if messageType == -1 && websocket.IsCloseError(err, websocket.CloseGoingAway, websocket.CloseNormalClosure, websocket.CloseNoStatusReceived) {
				Manager.DisConnect <- c
				return
			} else if messageType != websocket.PingMessage {
				return
			}
		}
	}()
}
