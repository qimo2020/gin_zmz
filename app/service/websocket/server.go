package websocket

import (
	"fmt"
	"time"

	"github.com/gorilla/websocket"
)

type MsgData struct {
	MsgType string      `json:"msg_type"`
	Data    interface{} `json:"data"`
	Msg     string      `json:"msg"`
	SendId  int         `json:"send_id"`
	GetId   int         `json:"get_id"`
}

//用户间通信通道
var ToClientChan chan *MsgData

//广播通道
var BroadcastChan chan *MsgData

var Manager = NewClientManager()

// 心跳间隔
var heartbeatInterval = 100 * time.Second

func init() {
	ToClientChan = make(chan *MsgData, 1000)
	BroadcastChan = make(chan *MsgData, 1000)
}

func StartWebsocket() {
	go Manager.Start()
	go WriteMessage()
}

//发送消息到用户
func SendMessageClient(sendId int, getId int, data interface{}) {
	ToClientChan <- &MsgData{
		MsgType: "friend_msg",
		Data:    data,
		Msg:     "好友发送消息",
		SendId:  sendId,
		GetId:   getId,
	}
}

func SendBroadcast(msg_type string, data interface{}) {
	BroadcastChan <- &MsgData{
		MsgType: msg_type,
		Data:    data,
		Msg:     "广播消息",
		SendId:  0,
		GetId:   0,
	}
}

//监听并发送给客户端信息
func WriteMessage() {
	for {
		select {
		case clientInfo := <-ToClientChan:
			if conn, err := Manager.GetByUserId(clientInfo.GetId); err == nil && conn != nil {
				if err := Render(conn.Socket, *clientInfo); err != nil {
					Manager.DisConnect <- conn
				}
			}
		case broadcastInfo := <-BroadcastChan:
			allClient := Manager.GetAllUser()
			for _, value := range allClient {
				if err := Render(value.Socket, *broadcastInfo); err != nil {
					Manager.DisConnect <- value
					fmt.Println(err)
				}
			}
		default:
		}
	}
}

//启动定时器进行心跳检测
func PingTimer() {
	go func() {
		ticker := time.NewTicker(heartbeatInterval)
		defer ticker.Stop()
		for {
			<-ticker.C
			//发送心跳
			for _, conn := range Manager.GetAllUser() {
				if err := conn.Socket.WriteControl(websocket.PingMessage, []byte{}, time.Now().Add(time.Second)); err != nil {
					Manager.DisConnect <- conn
				}
			}
		}

	}()
}

func Render(conn *websocket.Conn, msgData MsgData) error {
	return conn.WriteJSON(msgData)
}
