package red_pack

import(
	"gin_zmz/app/model"
	"gin_zmz/boot"
	"errors"
	"time"
	"gin_zmz/app/service/websocket"
	"strconv"
)

func GetRedPack(user_id int,red_pack_id int)(bool,error){
	var user_red model.UserRed
	boot.Db.Where("user_id = ? AND red_pack_id = ?",user_id,red_pack_id).First(&user_red)
	if(user_red.Id!=0){
		return false,errors.New("您已领取过此红包")
	}
	var red_pack model.RedPack
	boot.Db.Where("id=?",red_pack_id).First(&red_pack)
	if(red_pack.Stock<=0){
		return false,errors.New("红包已领取完")
	}

	save_data := model.UserRed{
		UserId 		: user_id,
		RedPackId	: red_pack_id,
		CreateTime	: time.Now().Unix(),
		UpdateTime	: time.Now().Unix(),
	}
	
	boot.Db.Exec(" UPDATE zmz_red_pack SET stock=stock-1 where id="+strconv.Itoa(red_pack_id))
	boot.Db.Create(&save_data)

	return true,nil
}


func CreateRedPack(name string,stock int) bool {
	var red_pack model.RedPack = model.RedPack{
		Name 	: name,
		Stock	: stock,
		CreateTime	: time.Now().Unix(),
		UpdateTime	: time.Now().Unix(),
	}
	boot.Db.Create(&red_pack)
	websocket.SendBroadcast("red",map[string]int{
		"red_pack_id": red_pack.Id,
	})

	return true
}