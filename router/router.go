package router

import (
	red_pack_controller "gin_zmz/app/api/red_pack"
	user_controller "gin_zmz/app/api/user"
	"gin_zmz/app/service/file"
	"gin_zmz/app/service/websocket"
	"gin_zmz/boot"
	"gin_zmz/middleware"

	"github.com/gin-gonic/gin"
)

func Run() {
	r := gin.Default()

	r.Use(middleware.Cros())
	{
		r.GET("/ws", websocket.Run)
		r.POST("/upload", file.UploadLocal)

		r.GET("/message", user_controller.Index)
		user := r.Group("/user")
		{
			user.POST("/regist", user_controller.Regist)
			user.POST("/login", user_controller.Login)
			user.GET("/info", middleware.CheckToken(), user_controller.Info)
			user.POST("/broadcast", user_controller.SendBroadcast)
		}
		red := r.Group("/red")
		{
			red.POST("", red_pack_controller.GetRedPack)
			red.POST("/create", red_pack_controller.CreateRedPack)
		}
	}

	r.Run(boot.HttpPort)
}
