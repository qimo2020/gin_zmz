package jwt

import (
    "time"
    "github.com/dgrijalva/jwt-go"
    "github.com/gin-gonic/gin"
)

//自定义一个字符串
var jwtkey = []byte("test1223321")
var str string

type Claims struct {
    UserId int
    jwt.StandardClaims
}

//颁发token
func Setting(user_id int) (string,error){
    expireTime := time.Now().Add(7 * 24 * time.Hour)
    claims := &Claims{
        UserId: user_id,
        StandardClaims: jwt.StandardClaims{
            ExpiresAt: expireTime.Unix(), //过期时间
            IssuedAt:  time.Now().Unix(),
            Issuer:    "zmz",  // 签名颁发者
            Subject:   "user-token", //签名主题
        },
    }
    token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
    // fmt.Println(token)
    tokenString, err := token.SignedString(jwtkey)
    if err != nil {
        return "",err
    }
    str = tokenString
	return str,nil
}

//解析token
func getting(ctx *gin.Context) int {
    tokenString := ctx.GetHeader("Authorization")
    //vcalidate token formate
    if tokenString == "" {
        ctx.JSON(200, gin.H{"code": 50001, "msg": "token不存在"})
        ctx.Abort()
    }
    token, claims, err := ParseToken(tokenString)
    if err != nil || !token.Valid {
        ctx.JSON(200, gin.H{"code": 50002, "msg": "token错误"})
        ctx.Abort()
	}
	return claims.UserId
}

func ParseToken(tokenString string) (*jwt.Token, *Claims, error) {
    Claims := &Claims{}
    token, err := jwt.ParseWithClaims(tokenString, Claims, func(token *jwt.Token) (i interface{}, err error) {
        return jwtkey, nil
    })
    return token, Claims, err
}