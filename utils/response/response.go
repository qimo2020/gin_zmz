package response

func Data(code int, msg interface{}, data interface{}) map[string]interface{} {
	return map[string]interface{}{
		"code": code,
		"msg":  msg,
		"data": data,
	}
}

var (
	// OK
	OK = NewError(0, "OK")

	// 服务级错误码
	ErrServer    = NewError(10001, "服务异常，请联系管理员")
	ErrParam     = NewError(10002, "参数有误")
	ErrSignParam = NewError(10003, "签名参数有误")

	// 模块级错误码 - 用户模块
	ErrUserPhone   = NewError(20101, "用户手机号不合法")
	ErrUserCaptcha = NewError(20102, "用户验证码有误")

	//文件上传错误
	ErrFile = NewError(30001, "文件上传错误")

	// 自定义错误内容
	ErrCustom = NewError(10031, "操作失败")
)

type err struct {
	Code int         `json:"code"` //业务编码
	Msg  string      `json:"msg"`  //错误描述
	Data interface{} `json:"data"` //成功时返回的数据
}

func NewError(code int, msg string) *err {
	return &err{
		Code: code,
		Msg:  msg,
		Data: nil,
	}
}

func (e *err) WithData(data interface{}) *err {
	e.Data = data
	return e
}

func (e *err) WithMsg(data string) *err {
	e.Msg = data
	return e
}
