package helper

import(
	"math"
	"crypto/md5"
	"encoding/hex"
	"math/rand"
	"os"
)


func BuildPage(page int , rownum int , total int) map[string]int{
	if(page==0){
		page = 1
	}
	if(rownum==0){
		rownum = 10
	}
	page_total := math.Ceil(float64(total)/float64(rownum))
	return map[string]int{
		"page" 		: page,
		"rownum"	: rownum,
		"total"		: total,
		"page_total": int(page_total),
	}
}


func Md5(str string) string {
	h := md5.New()
    h.Write([]byte(str))
    cipherStr := h.Sum(nil)
	return hex.EncodeToString(cipherStr)
}


// RandUp 随机字符串，包含 英文字母和数字附加=_两个符号
func RandStr(n int) string {
	if n <= 0 {
		return ""
	}
	b := make([]byte, n)
	arc := uint8(0)
	if _, err := rand.Read(b[:]); err != nil {
		return ""
	}
	for i, x := range b {
		arc = x & 63
		b[i] = []byte("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=_")[arc]
	}
	return string(b) 
}

//判断文件夹是否存在
func FileIsExist(path string) bool {
    _, err := os.Stat(path)
    if err == nil {
        return false
    }
    if os.IsNotExist(err) {
        return false
    }
    return true
}

func getFile(id int){

}