package logger

import(
	"github.com/sirupsen/logrus"
	"os"
)

var Log = logrus.New()

func init(){
	Log.SetOutput(os.Stdout)
	Log.Formatter = new(logrus.JSONFormatter)
	Log.Formatter.(*logrus.JSONFormatter).DisableTimestamp = true 
}