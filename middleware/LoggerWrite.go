package middleware

import (
	"fmt"
	"gin_zmz/boot"

	"github.com/gin-gonic/gin"
)

func LoggerWrite(c *gin.Context) gin.HandlerFunc {
	return func(c *gin.Context) {
		fmt.Println(c.Params)
		boot.ZapLog.Info("[请求头]：")
		boot.ZapLog.Info("[请求参数]：")
		c.Next()
	}
}
