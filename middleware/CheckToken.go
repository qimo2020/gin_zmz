package middleware

import(
	"github.com/gin-gonic/gin"
	"gin_zmz/utils/jwt"
)

func CheckToken() gin.HandlerFunc {
	return func(ctx *gin.Context){
    	tokenString := ctx.GetHeader("Authorization")
    	//vcalidate token formate
    	if tokenString == "" {
        	ctx.JSON(200, gin.H{"code": 50001, "msg": "token不存在"})
			ctx.Abort()
			return
    	}
    	token, claims, err := jwt.ParseToken(tokenString)
    	if err != nil || !token.Valid {
        	ctx.JSON(200, gin.H{"code": 50002, "msg": "token错误"})
			ctx.Abort()
			return
		}
		ctx.Set("user_id",claims.UserId)
		ctx.Next()
	}
}