package middleware

import(
	"github.com/gin-gonic/gin"
)

func Cros() gin.HandlerFunc {
    return func(c *gin.Context) {
        method := c.Request.Method

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization,X-Token")
		c.Header("Access-Control-Allow-Methods", "POST,GET,PUT,DELETE,OPTIONS")
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")

        //放行所有OPTIONS方法
        if method == "OPTIONS" {
			c.JSON(200, "Options Request!")
			c.Abort()
			return
        }

        c.Next()
    }
}