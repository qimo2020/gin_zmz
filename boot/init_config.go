package boot

import (
	"github.com/spf13/viper"
	// "golang.org/x/net/context"
	"log"

	"github.com/fsnotify/fsnotify"
	// "github.com/jinzhu/gorm"
	// _ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	//debug模式
	AppDebug bool
	//网站端口号
	HttpPort string

	MysqlConfig mysql_type

	Upload_MaxSize int

	Upload_Path string

	//日志配置
	LoggerConfig logger_config
)

type mysql_type struct {
	host   string
	port   string
	user   string
	pass   string
	char   string
	prex   string
	dbname string
}

type logger_config struct {
	GinPath    string
	MyPath     string
	TextFormat string
	MaxSize    int
	MaxBackups int
	MaxAge     int
}

//初始化获取所有的配置信息
func initConfig() {

	v := viper.New()

	readToml(v)

	//监听回调函数
	v.OnConfigChange(func(e fsnotify.Event) {
		readToml(v)
		log.Printf("Config file is changed: %s \n", e.String())
	})
	v.WatchConfig()

}

func readToml(v *viper.Viper) {
	v.SetConfigName("app")

	v.AddConfigPath("config")

	v.SetConfigType("toml")

	if err := v.ReadInConfig(); err != nil {
		log.Printf("Config file is changed: %s \n", err.Error())
	}

	AppDebug = v.GetBool("server.AppDebug")

	HttpPort = v.GetString("server.HttpPort")

	MysqlConfig = mysql_type{
		host:   v.GetString("database.Host"),
		port:   v.GetString("database.Port"),
		user:   v.GetString("database.User"),
		pass:   v.GetString("database.Pass"),
		char:   v.GetString("database.Char"),
		prex:   v.GetString("database.Prex"),
		dbname: v.GetString("database.DbName"),
	}

	LoggerConfig = logger_config{
		GinPath:    v.GetString("logger.GinPath"),
		MyPath:     v.GetString("logger.MyPath"),
		TextFormat: v.GetString("logger.TextFormat"),
		MaxSize:    v.GetInt("logger.MaxSize"),
		MaxBackups: v.GetInt("logger.MaxBackups"),
		MaxAge:     v.GetInt("logger.MaxAge"),
	}

	Upload_MaxSize = v.GetInt("upload.MaxSize")
	Upload_Path = v.GetString("upload.Path")
}
