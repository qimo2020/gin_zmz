package boot

import (
	"log"
	"time"

	"gorm.io/driver/mysql"
	_ "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

var Db *gorm.DB
var err error

func connDb() {

	dsn := MysqlConfig.user + ":" + MysqlConfig.pass + "@tcp(" + MysqlConfig.host + ":" + MysqlConfig.port + ")/" + MysqlConfig.dbname + "?charset=" + MysqlConfig.char + "&parseTime=True&loc=Local"

	Db, err = gorm.Open(mysql.New(mysql.Config{
		DSN:               dsn,
		DefaultStringSize: 256,
	}), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix:   MysqlConfig.prex,
			SingularTable: true,
		},
	})

	if err != nil {
		log.Fatalln("数据库连接错误", err)
	}
	// 获取通用数据库对象 sql.DB ，然后使用其提供的功能
	sqlDB, _ := Db.DB()
	// SetMaxIdleConns 设置空闲连接池中连接的最大数量
	sqlDB.SetMaxIdleConns(10)
	// SetMaxOpenConns 设置打开数据库连接的最大数量。
	sqlDB.SetMaxOpenConns(100)
	// SetConnMaxLifetime 设置了连接可复用的最大时间。
	sqlDB.SetConnMaxLifetime(time.Hour)

}
