package boot

import (
	service_ws "gin_zmz/app/service/websocket"
)

func Init() {
	//初始化配置信息
	initConfig()
	//初始化数据库连接
	connDb()
	//初始化日志
	initLog()
	//websocket开启
	service_ws.StartWebsocket()
}
